﻿using WippinApi.Core;
using WippinApi.Core.Repositories;
using WippinApi.Persistence.Repositories;

namespace WippinApi.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WippinContext _context;
        public UnitOfWork(WippinContext wippinContext)
        {
            _context = wippinContext;
            Productos = new ProductoRepository(_context);
            Imagenes = new ImagenRepository(_context);
            Categorias = new CategoriasRepository(_context);
            Vistas = new VistaRepository(_context);
            Transacciones = new TransaccionRepository(_context);
            Ciudades = new CiudadesRepository(_context);
            Valoraciones = new ValoracionRepository(_context);
        }

        public IProductoRepository Productos { get; private set; }
        public IImagenRepository Imagenes { get; private set; }
        public ICategoriaRepository Categorias { get; }
        public IVistaRepository Vistas { get; }
        public ITransaccionRepository Transacciones { get; set; }
        public ICiudadRepository Ciudades { get; set; }
        public IValoracionRepository Valoraciones { get; set; }
        public void Dispose()
        {
            _context.Dispose();

        }

        public int Complete()
        {
            return _context.SaveChanges();
        }
    }
}