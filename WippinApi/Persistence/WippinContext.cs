﻿//using WippinApi.Core.Entities;
using System.Data.Entity;
using WippinApi.Core.Entities;
using WippinApi.Core.EntityConfigurations;

namespace WippinApi.Persistence
{
    public class WippinContext : DbContext
    {
        public WippinContext()
            : base("name=WippinContext")
        {
            this.Configuration.ProxyCreationEnabled = true;
            this.Configuration.LazyLoadingEnabled = false;
        }

        public static WippinContext Create()
        {
            return new WippinContext();
        }

        public virtual DbSet<Categoria> Categorias { get; set; }
        public virtual DbSet<Ciudad> Ciudades { get; set; }
        public virtual DbSet<Imagen> Imagenes { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<Transaccion> Transacciones { get; set; }
        public virtual DbSet<Ubicacion> Ubicaciones { get; set; }
        public virtual DbSet<Valoracion> Valoraciones { get; set; }
        public virtual DbSet<Vista> Vistas { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }





        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new CategoriaConfiguration());
            modelBuilder.Configurations.Add(new CiudadConfiguration());
            modelBuilder.Configurations.Add(new ImagenConfiguration());
            modelBuilder.Configurations.Add(new ProductoConfiguration());
            modelBuilder.Configurations.Add(new TransaccionConfiguration());
            modelBuilder.Configurations.Add(new UbicacionConfiguration());
            modelBuilder.Configurations.Add(new ValoracionConfiguration());
            modelBuilder.Configurations.Add(new VistaConfiguration());


        }

        //public class ApplicationUser : IdentityUser
        //{
        //    public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        //    {
        //        // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
        //        var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ExternalCookie);

        //        //userIdentity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
        //        userIdentity.AddClaim(new Claim(ClaimTypes.Role, "user"));
        //        //userIdentity.AddClaim(new Claim("sub", context.UserName));
        //        // Agregar aquí notificaciones personalizadas de usuario
        //        return userIdentity;
        //    }
        //}

    }
}