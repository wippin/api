﻿using WippinApi.Core.Entities;
using WippinApi.Core.Repositories;

namespace WippinApi.Persistence.Repositories
{
    public class CategoriasRepository : Repository<Categoria>, ICategoriaRepository
    {
        public CategoriasRepository(WippinContext context) : base(context)
        {

        }
    }
}