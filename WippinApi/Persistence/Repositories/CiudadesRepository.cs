﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WippinApi.Core.Entities;
using WippinApi.Core.Repositories;

namespace WippinApi.Persistence.Repositories
{
    public class CiudadesRepository : Repository<Ciudad>, ICiudadRepository
    {
        public CiudadesRepository(WippinContext context) : base(context)
        {

        }
    }
}