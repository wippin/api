﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WippinApi.Core.Entities;
using WippinApi.Core.Repositories;

namespace WippinApi.Persistence.Repositories
{
    public class ValoracionRepository : Repository<Valoracion>, IValoracionRepository
    {
        public ValoracionRepository(WippinContext context) : base(context)
        {

        }
    }
}