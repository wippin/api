﻿using WippinApi.Core.Entities;
using WippinApi.Core.Repositories;

namespace WippinApi.Persistence.Repositories
{
    public class ImagenRepository : Repository<Imagen>, IImagenRepository
    {
        public ImagenRepository(WippinContext context) : base(context)
        {
        }
    }
}