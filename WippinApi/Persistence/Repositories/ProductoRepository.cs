﻿using System.Collections;
using System.Data.Entity;
using System.Threading.Tasks;
using WippinApi.Core.Entities;
using WippinApi.Core.Repositories;

namespace WippinApi.Persistence.Repositories
{
    public class ProductoRepository : Repository<Producto>, IProductoRepository
    {
        private readonly WippinContext _context;

        public ProductoRepository(WippinContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IList> GetAllProducts()
        {
            return await _context.Productos
                .Include(p => p.Categoria)
                .Include(p => p.Ubicacion)
                .Include(p => p.Imagenes).ToListAsync();

        }
    }
}