﻿using WippinApi.Core.Entities;
using WippinApi.Core.Repositories;

namespace WippinApi.Persistence.Repositories
{
    public class VistaRepository : Repository<Vista>, IVistaRepository
    {
        public VistaRepository(WippinContext context) : base(context)
        {
        }
    }
}