﻿using System.Data.Entity;
using WippinApi.Core.Entities;
using WippinApi.Core.Repositories;

namespace WippinApi.Persistence.Repositories
{
    public class TransaccionRepository : Repository<Transaccion>, ITransaccionRepository
    {
        public TransaccionRepository(DbContext context) : base(context)
        {

        }
    }
}