﻿using System.Data.Entity.ModelConfiguration;
using WippinApi.Core.Entities;

namespace WippinApi.Core.EntityConfigurations
{
    public class VistaConfiguration : EntityTypeConfiguration<Vista>
    {
        public VistaConfiguration()
        {
            HasKey(v => v.Id);

            HasRequired(v => v.Producto).WithMany().HasForeignKey(v => v.IdProducto);
        }
    }
}