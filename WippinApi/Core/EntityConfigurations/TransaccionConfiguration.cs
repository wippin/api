﻿using System.Data.Entity.ModelConfiguration;
using WippinApi.Core.Entities;

namespace WippinApi.Core.EntityConfigurations
{
    public class TransaccionConfiguration : EntityTypeConfiguration<Transaccion>
    {
        public TransaccionConfiguration()
        {
            ToTable("Transacciones");
            HasKey(t => t.Id);

            Property(t => t.IdUsuarioConsumidor).IsRequired();

            Property(t => t.IdUsuarioPrestador).IsRequired();

            Property(t => t.Titulo)
                .IsRequired()
                .HasMaxLength(250);

            Property(t => t.Precio)
                .IsRequired();

            Property(t => t.Descripcion)
                .IsRequired();

            //HasRequired(t => t.Contacto)
            //    .WithRequiredPrincipal(c => c.Transaccion);

            HasOptional(t => t.Valoracion)
                .WithOptionalPrincipal(v => v.Transaccion);


            //public DateTime FechaTransaccion { get; set; }
            //public DateTime FechaEstimada { get; set; }
            //public Valoracion Valoracion { get; set; }
        }
    }
}