﻿using System.Data.Entity.ModelConfiguration;
using WippinApi.Core.Entities;

namespace WippinApi.Core.EntityConfigurations
{
    public class ValoracionConfiguration : EntityTypeConfiguration<Valoracion>
    {
        public ValoracionConfiguration()
        {
            ToTable("Valoraciones");
            HasKey(v => v.Id);

            Property(v => v.Comentario)
                .HasMaxLength(300);


            HasOptional(t => t.Transaccion)
                .WithOptionalDependent(v => v.Valoracion).Map(m =>
                {
                    m.MapKey("IdTransaccion");
                });

        }
    }
}