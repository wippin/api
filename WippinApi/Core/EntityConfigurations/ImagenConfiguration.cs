﻿using System.Data.Entity.ModelConfiguration;
using WippinApi.Core.Entities;

namespace WippinApi.Core.EntityConfigurations
{
    public class ImagenConfiguration : EntityTypeConfiguration<Imagen>
    {
        public ImagenConfiguration()
        {
            ToTable("Imagenes");
            HasKey(i => i.Id);

            Property(i => i.Url).IsRequired();

            Property(i => i.IdProducto)
                .IsRequired();
        }
    }
}