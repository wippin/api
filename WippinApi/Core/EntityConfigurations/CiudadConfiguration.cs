﻿using System.Data.Entity.ModelConfiguration;
using WippinApi.Core.Entities;

namespace WippinApi.Core.EntityConfigurations
{
    public class CiudadConfiguration : EntityTypeConfiguration<Ciudad>
    {
        public CiudadConfiguration()
        {
            ToTable("Ciudades");
            HasKey(c => c.Id);

            Property(c => c.Nombre)
                .IsRequired()
                .HasMaxLength(100);

            //HasMany(p => p.Ubicaciones)
            // .WithOptional(i => i.Ciudad)
            // .HasForeignKey(i => i.IdCiudad);
        }

    }
}