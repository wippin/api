﻿using System.Data.Entity.ModelConfiguration;
using WippinApi.Core.Entities;

namespace WippinApi.Core.EntityConfigurations
{
    public class UbicacionConfiguration : EntityTypeConfiguration<Ubicacion>
    {
        public UbicacionConfiguration()
        {
            ToTable("Ubicaciones");
            HasKey(u => u.Id);



            HasOptional(u => u.Producto).WithOptionalDependent(p => p.Ubicacion).Map(m =>
            {
                m.MapKey("IdProducto");
            });

            HasOptional(u => u.Ciudad)
                .WithMany(c => c.Ubicaciones)
                .HasForeignKey(u => u.IdCiudad);
        }
    }
}