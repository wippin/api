﻿using System.Data.Entity.ModelConfiguration;
using WippinApi.Core.Entities;

namespace WippinApi.Core.EntityConfigurations
{
    public class ProductoConfiguration : EntityTypeConfiguration<Producto>
    {
        public ProductoConfiguration()
        {
            ToTable("Productos");
            HasKey(p => p.Id);

            Property(p => p.Titulo).IsRequired();

            Property(p => p.IdUsuario).IsRequired();

            Property(p => p.EmailContacto).IsRequired();

            HasMany(p => p.Imagenes)
                .WithRequired(i => i.Producto)
                .HasForeignKey(i => i.IdProducto);


            HasOptional(p => p.Ubicacion)
                .WithOptionalPrincipal(u => u.Producto);

            HasRequired(p => p.Categoria).WithMany().HasForeignKey(p => p.IdCategoria);

        }
    }
}