﻿using System.ComponentModel.DataAnnotations;

namespace WippinApi.Core.Entities
{
    public class Contact
    {
        [Key]
        public long ContactId { get; set; }

        public string Email { get; set; }
    }
}