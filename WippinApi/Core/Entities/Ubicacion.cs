﻿using Newtonsoft.Json;

namespace WippinApi.Core.Entities
{
    public class Ubicacion
    {
        public long Id { get; set; }
        [JsonIgnore]
        public virtual Producto Producto { get; set; }
        public Ciudad Ciudad { get; set; }
        public long? IdCiudad { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
    }
}