﻿using System;

namespace WippinApi.Core.Entities
{
    public class Transaccion
    {

        public long Id { get; set; }
        public string IdUsuarioConsumidor { get; set; }
        public string IdUsuarioPrestador { get; set; }
        public string Titulo { get; set; }
        public double Precio { get; set; }
        public string Descripcion { get; set; }
        public string EmailContacto { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public DateTime FechaEstimada { get; set; }
        public Valoracion Valoracion { get; set; }
    }
}