﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace WippinApi.Core.Entities
{
    public class Imagen
    {
        public long Id { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Producto Producto { get; set; }
        public long IdProducto { get; set; }
        public string Url { get; set; }
    }
}