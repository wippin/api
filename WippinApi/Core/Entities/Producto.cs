﻿using System.Collections.Generic;

namespace WippinApi.Core.Entities
{
    public class Producto
    {
        public long Id { get; set; }
        public string Titulo { get; set; }
        public double Precio { get; set; }
        public string Descripcion { get; set; }
        public string EmailContacto { get; set; }
        public virtual IList<Imagen> Imagenes { get; set; }
        public virtual Categoria Categoria { get; set; }
        public long IdCategoria { get; set; }
        public virtual Ubicacion Ubicacion { get; set; }
        public bool Pausado { get; set; }
        public string IdUsuario { get; set; }
    }
}