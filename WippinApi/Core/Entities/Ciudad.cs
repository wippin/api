﻿using System.Collections.Generic;

namespace WippinApi.Core.Entities
{
    public class Ciudad
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual IList<Ubicacion> Ubicaciones { get; set; }
    }
}