﻿namespace WippinApi.Core.Entities
{
    public class Valoracion
    {
        public long Id { get; set; }
        public Transaccion Transaccion { get; set; }
        public string Comentario { get; set; }
        public Calificacion Calificacion { get; set; }
    }
}