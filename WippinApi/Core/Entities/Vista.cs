﻿namespace WippinApi.Core.Entities
{
    public class Vista
    {
        public long Id { get; set; }
        public Producto Producto { get; set; }
        public long IdProducto { get; set; }
    }
}