﻿namespace WippinApi.Core.Entities
{
    public class Categoria
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
    }

}