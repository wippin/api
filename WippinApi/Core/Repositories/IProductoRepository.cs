﻿using System.Collections;
using System.Threading.Tasks;
using WippinApi.Core.Entities;

namespace WippinApi.Core.Repositories
{
    public interface IProductoRepository : IRepository<Producto>
    {

        Task<IList> GetAllProducts();
    }
}
