﻿using WippinApi.Core.Entities;

namespace WippinApi.Core.Repositories
{
    public interface IImagenRepository : IRepository<Imagen>
    {
    }
}
