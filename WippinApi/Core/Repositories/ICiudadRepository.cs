﻿using WippinApi.Core.Entities;

namespace WippinApi.Core.Repositories
{
    public interface ICiudadRepository : IRepository<Ciudad>
    {
    }
}
