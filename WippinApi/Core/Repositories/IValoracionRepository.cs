﻿using WippinApi.Core.Entities;

namespace WippinApi.Core.Repositories
{
    public interface IValoracionRepository :IRepository<Valoracion>
    {
    }
}
