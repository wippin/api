﻿using WippinApi.Core.Entities;

namespace WippinApi.Core.Repositories
{
    public interface IVistaRepository : IRepository<Vista> { }
}