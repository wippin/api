﻿using System;
using WippinApi.Core.Repositories;

namespace WippinApi.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IProductoRepository Productos { get; }
        IImagenRepository Imagenes { get; }
        ICategoriaRepository Categorias { get; }
        IVistaRepository Vistas { get; }

        int Complete();
    }
}
