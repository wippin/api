namespace WippinApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedVista : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Vistas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdProducto = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Productos", t => t.IdProducto, cascadeDelete: true)
                .Index(t => t.IdProducto);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vistas", "IdProducto", "dbo.Productos");
            DropIndex("dbo.Vistas", new[] { "IdProducto" });
            DropTable("dbo.Vistas");
        }
    }
}
