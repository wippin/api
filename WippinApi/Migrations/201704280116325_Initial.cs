namespace WippinApi.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categorias",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Nombre = c.String(nullable: false, maxLength: 250),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Productos",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Titulo = c.String(nullable: false),
                    Precio = c.Double(nullable: false),
                    Descripcion = c.String(),
                    IdCategoria = c.Long(nullable: false),
                    Pausado = c.Boolean(nullable: false),
                    IdUsuario = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categorias", t => t.IdCategoria)
                .Index(t => t.IdCategoria);

            CreateTable(
                "dbo.Imagenes",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    IdProducto = c.Long(nullable: false),
                    Url = c.String(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Productos", t => t.IdProducto, cascadeDelete: true)
                .Index(t => t.IdProducto);

            CreateTable(
                "dbo.Ubicaciones",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    IdCiudad = c.Long(),
                    Latitud = c.Double(nullable: false),
                    Longitud = c.Double(nullable: false),
                    IdProducto = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ciudades", t => t.IdCiudad)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdCiudad)
                .Index(t => t.IdProducto);

            CreateTable(
                "dbo.Ciudades",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Nombre = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Transacciones",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    IdUsuarioConsumidor = c.String(nullable: false),
                    IdUsuarioPrestador = c.String(nullable: false),
                    Titulo = c.String(nullable: false, maxLength: 250),
                    Precio = c.Double(nullable: false),
                    Descripcion = c.String(nullable: false),
                    FechaTransaccion = c.DateTime(nullable: false),
                    FechaEstimada = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Contactos",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Email = c.String(maxLength: 250),
                    IdTransaccion = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transacciones", t => t.IdTransaccion)
                .Index(t => t.IdTransaccion);

            CreateTable(
                "dbo.Valoraciones",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Comentario = c.String(maxLength: 300),
                    Calificacion = c.Int(nullable: false),
                    IdTransaccion = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transacciones", t => t.IdTransaccion)
                .Index(t => t.IdTransaccion);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Valoraciones", "IdTransaccion", "dbo.Transacciones");
            DropForeignKey("dbo.Contactos", "IdTransaccion", "dbo.Transacciones");
            DropForeignKey("dbo.Productos", "IdCategoria", "dbo.Categorias");
            DropForeignKey("dbo.Ubicaciones", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.Ubicaciones", "IdCiudad", "dbo.Ciudades");
            DropForeignKey("dbo.Imagenes", "IdProducto", "dbo.Productos");
            DropIndex("dbo.Valoraciones", new[] { "IdTransaccion" });
            DropIndex("dbo.Contactos", new[] { "IdTransaccion" });
            DropIndex("dbo.Ubicaciones", new[] { "IdProducto" });
            DropIndex("dbo.Ubicaciones", new[] { "IdCiudad" });
            DropIndex("dbo.Imagenes", new[] { "IdProducto" });
            DropIndex("dbo.Productos", new[] { "IdCategoria" });
            DropTable("dbo.Valoraciones");
            DropTable("dbo.Contactos");
            DropTable("dbo.Transacciones");
            DropTable("dbo.Ciudades");
            DropTable("dbo.Ubicaciones");
            DropTable("dbo.Imagenes");
            DropTable("dbo.Productos");
            DropTable("dbo.Categorias");
        }
    }
}
