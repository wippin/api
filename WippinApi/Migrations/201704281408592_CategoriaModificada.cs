namespace WippinApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoriaModificada : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Productos", "IdCategoria", "dbo.Categorias");
            AddForeignKey("dbo.Productos", "IdCategoria", "dbo.Categorias", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productos", "IdCategoria", "dbo.Categorias");
            AddForeignKey("dbo.Productos", "IdCategoria", "dbo.Categorias", "Id");
        }
    }
}
