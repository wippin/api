namespace WippinApi.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class cambios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productos", "EmailContacto", c => c.String(nullable: false));
            AddColumn("dbo.Transacciones", "EmailContacto", c => c.String());
        }

        public override void Down()
        {


            DropColumn("dbo.Transacciones", "EmailContacto");
            DropColumn("dbo.Productos", "EmailContacto");
        }
    }
}
