﻿using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using WippinApi.Core.Entities;
using WippinApi.Persistence;

namespace WippinApi.Controllers
{
    [RoutePrefix("api/categorias")]
    public class CategoriasController : BaseApiController
    {
        [SwaggerResponse(typeof(IEnumerable<Categoria>))]
        [HttpGet]
        [Route("", Name = "GetCategories")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                return Ok(unitOfWork.Categorias.GetAll());
            }
        }


        [SwaggerResponse(typeof(Categoria))]
        [HttpPost]
        [Route("", Name = "AddCategorie")]
        public async Task<IHttpActionResult> Create(Categoria categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                try
                {
                    unitOfWork.Categorias.Add(categoria);
                    unitOfWork.Complete();
                    return Ok(categoria);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                    return BadRequest(ModelState);
                }
            }
        }

        [HttpPost]
        [Route("createcontact/{contact}", Name = "createcontact")]
        public async Task<IHttpActionResult> createcontact(string contact)
        {
            var contactObject = new Contact();
            contactObject.Email = contact.Replace(",", ".");
            this.WippinContext.Contacts.Add(contactObject);
            this.WippinContext.SaveChanges();
            return Ok();
        }


        [SwaggerResponse(typeof(void))]
        [HttpDelete]
        [Route("{id:long}", Name = "DeleteCategorie")]
        public async Task<IHttpActionResult> Delete(long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                try
                {
                    var categoria = unitOfWork.Categorias.Get(id);
                    if (categoria == null) return NotFound();
                    unitOfWork.Categorias.Remove(categoria);
                    unitOfWork.Complete();
                    return Ok();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                    return BadRequest(ModelState);
                }
            }
        }
    }
}
