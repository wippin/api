﻿using Microsoft.AspNet.Identity.Owin;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using WippinApi.Persistence;

namespace WippinApi.Controllers
{
    public class BaseApiController : ApiController
    {
        private readonly WippinContext _wippinContext;



        protected const int ItemsPerPage = 10;


        protected WippinContext WippinContext => _wippinContext ?? Request.GetOwinContext().Get<WippinContext>();

    }
    public class NotModified : IHttpActionResult
    {
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage(HttpStatusCode.NotModified);
            return Task.FromResult(response);
        }
    }
}