﻿using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WippinApi.Core.Entities;
using WippinApi.Persistence;

namespace WippinApi.Controllers
{
    [RoutePrefix("api/usuarios")]
    public class UsuariosController : BaseApiController
    {
        [Authorize]
        [SwaggerResponse(typeof(IEnumerable<Producto>))]
        [HttpGet]
        [Route("", Name = "GetProductos")]
        public async Task<IHttpActionResult> GetProductos()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                try
                {
                    string idUsuarioConsumidor = "";
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    foreach (var item in claimsIdentity.Claims)
                    {
                        if (item.Type == ClaimTypes.NameIdentifier)
                        {
                            idUsuarioConsumidor = item.Value;
                            break;
                        }
                    }
                    if (idUsuarioConsumidor.Equals(""))
                    {
                        ModelState.AddModelError("", "Usuario logueado no encontrado.");
                        return BadRequest(ModelState);
                    }
                    var productos = unitOfWork.Productos.GetAll().Where(p => p.IdUsuario == idUsuarioConsumidor).ToList();
                    unitOfWork.Complete();
                    return Ok(productos);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                    return BadRequest(ModelState);
                }
            }
        }
    }
}
