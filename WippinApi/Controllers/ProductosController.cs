﻿using Newtonsoft.Json;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WippinApi.Core.Entities;
using WippinApi.Persistence;
using WippinApi.Services;
using ClaimTypes = System.IdentityModel.Claims.ClaimTypes;

// ReSharper disable AccessToDisposedClosure

namespace WippinApi.Controllers
{
    [RoutePrefix("api/productos")]
    public class ProductosController : BaseApiController
    {
        [SwaggerResponse(typeof(object))]
        [HttpGet]
        [Route("", Name = "GetPaginated")]
        public async Task<IHttpActionResult> GetPaginated(bool paging = true, int page = 1, int pageSize = ItemsPerPage)
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {

                var items =
                    unitOfWork.Productos.GetAllAsQueryable()
                        .Include(p => p.Categoria)
                        .Include(p => p.Imagenes)
                        .Include(p => p.Ubicacion);
                var result = new PagedList<Producto>(items.OrderByDescending(p => p.Id), page, pageSize);
                return Ok(new
                {
                    TotalCount = result.TotalItemCount,
                    result.TotalPages,
                    result.HasPreviousPage,
                    result.HasNextPage,
                    Results = (await result.Items.ToListAsync())
                });
            }

        }

        [Authorize]
        [SwaggerResponse(typeof(IEnumerable<Producto>))]
        [HttpGet]
        [Route("GetProductsByUser", Name = "GetProductsByUser")]
        public async Task<IHttpActionResult> GetProductsByUser()
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var userId = claimsIdentity?.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

                var items =
                    unitOfWork.Productos.GetAllAsQueryable()
                        .Include(p => p.Categoria)
                        .Include(p => p.Imagenes)
                        .Include(p => p.Ubicacion)
                        .Where(p => p.IdUsuario == userId).ToListAsync();
                return Ok(await items);
            }

        }



        [SwaggerResponse(typeof(Producto))]
        [HttpGet]
        [Route("{id:long}", Name = "GetPostById")]
        public async Task<IHttpActionResult> Get(long id)
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                var producto = await this.WippinContext.Productos
                    .Include(p => p.Categoria)
                    .Include(p => p.Ubicacion)
                    .Include(p => p.Imagenes)
                    .Where(p => p.Id == id).SingleOrDefaultAsync();

                if (producto == null)
                {
                    return NotFound();
                }
                var vista = new Vista { Producto = producto };
                unitOfWork.Vistas.Add(vista);
                unitOfWork.Complete();
                return Ok(producto);
            }
        }


        [SwaggerResponse(typeof(IEnumerable<Producto>))]
        [HttpPost]
        [Route("search", Name = "Search")]
        public async Task<IHttpActionResult> GetFilteredBy(FilterParams filterParams)
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                if ((filterParams.IdCategoria == null || filterParams.IdCategoria == 0) &&
                    String.IsNullOrEmpty(filterParams.Texto) && filterParams.Ubicacion == null)
                {
                    var productosByCategoria = await this.WippinContext.Productos
                          .Include(p => p.Categoria)
                     .Include(p => p.Ubicacion)
                     .Include(p => p.Imagenes)
                         .ToListAsync();

                    if (filterParams.Ascendant != null)
                    {

                        productosByCategoria = (bool)filterParams.Ascendant ? productosByCategoria.OrderBy(p => p.Id).ToList() : productosByCategoria.OrderByDescending(p => p.Id).ToList();
                    }

                    return Ok(productosByCategoria);
                }
                if (filterParams.IdCategoria != null)
                {
                    var productosByCategoria = await this.WippinContext.Productos
                         .Include(p => p.Categoria)
                    .Include(p => p.Ubicacion)
                    .Include(p => p.Imagenes)
                        .Where(p => p.IdCategoria == filterParams.IdCategoria).ToListAsync();

                    if (productosByCategoria.Count > 0 && filterParams?.Texto?.Length > 0)
                    {

                        var productosByTexto =
                            productosByCategoria.Where(p => p.Titulo.ToLowerInvariant().Contains(filterParams.Texto.ToLowerInvariant()) || p.Descripcion.ToLowerInvariant().Contains(filterParams.Texto.ToLowerInvariant())).ToList();

                        if (filterParams.Ubicacion != null && productosByTexto.Count > 0)
                        {
                            try
                            {
                                var productosByUbicacion =
                                    productosByTexto.Where(p => p.Ubicacion.IdCiudad == filterParams.Ubicacion?.IdCiudad).ToList();
                                if (productosByUbicacion.Count > 0)
                                {
                                    if (filterParams.Ascendant != null)
                                    {

                                        productosByUbicacion = (bool)filterParams.Ascendant ? productosByUbicacion.OrderBy(p => p.Id).ToList() : productosByUbicacion.OrderByDescending(p => p.Id).ToList();
                                    }
                                    return Ok(productosByUbicacion);
                                }
                            }
                            catch (Exception)
                            {

                                return Ok(new List<object>());

                            }


                            if (productosByTexto.Count > 0)
                            {
                                if (filterParams.Ascendant != null)
                                {

                                    productosByTexto = (bool)filterParams.Ascendant ? productosByTexto.OrderBy(p => p.Id).ToList() : productosByTexto.OrderByDescending(p => p.Id).ToList();
                                }
                                return Ok(productosByTexto);
                            }

                        }
                        else
                        {
                            if (filterParams.Ascendant != null)
                            {

                                productosByTexto = (bool)filterParams.Ascendant ? productosByTexto.OrderBy(p => p.Id).ToList() : productosByTexto.OrderByDescending(p => p.Id).ToList();


                            }
                            return Ok(productosByTexto);
                        }
                    }
                    else
                    {
                        if (filterParams.Ascendant != null)
                        {

                            productosByCategoria = (bool)filterParams.Ascendant ? productosByCategoria.OrderBy(p => p.Id).ToList() : productosByCategoria.OrderByDescending(p => p.Id).ToList();
                            if (filterParams.Ubicacion != null)
                            {
                                productosByCategoria =
                                    productosByCategoria.Where(
                                        (p => p.Ubicacion.IdCiudad == filterParams.Ubicacion.IdCiudad)).ToList();
                            }
                        }
                        return Ok(productosByCategoria);
                    }

                }

                return Ok(new List<object>());

            }
        }

        [SwaggerResponse(typeof(Producto))]
        [HttpPost]
        [Route("update", Name = "Update")]
        public async Task<IHttpActionResult> Update(Producto producto)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "El formato de Producto no es válido");
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                var entity = await unitOfWork.Productos.GetAsync(producto.Id);
                if (entity == null)
                {
                    return NotFound();
                }
                try
                {
                    entity = producto;

                    unitOfWork.Complete();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return Ok(entity);
            }
        }

        [SwaggerResponse(typeof(IEnumerable<Producto>))]
        [HttpGet]
        [Route("mostviewed", Name = "GetMostViewed")]
        public async Task<IHttpActionResult> GetMostViewed()
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {

                var productoMasVistos = (await
                    unitOfWork.Vistas.GetAllAsQueryable()
                    .Include(v => v.Producto)
                    .Include(p => p.Producto.Categoria)
                    .Include(v => v.Producto.Ubicacion)
                    .Include(v => v.Producto.Imagenes)
                    .ToListAsync())
                        .GroupBy(v => new { v.IdProducto, Producto = v.Producto })
                        .Select(x => new
                        {
                            IdProducto = x.Key.IdProducto,
                            Producto = x.Key.Producto,
                            Total = x.Count()
                        }).OrderByDescending(x => x.Total).Select(x => x.Producto).Take(6).ToList();

                if (productoMasVistos.Count < 6)
                {
                    productoMasVistos = (await
                        unitOfWork.Productos.GetAllAsQueryable()
                            .Include(p => p.Categoria)
                            .Include(v => v.Ubicacion)
                            .Include(v => v.Imagenes)
                            .ToListAsync());
                }


                return Ok(productoMasVistos);
            }
        }


        //[Authorize]
        [SwaggerResponse(typeof(Producto))]
        [HttpPost]
        [Route("", Name = "CreateProd")]
        public async Task<IHttpActionResult> Create()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                //throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                ModelState.AddModelError("", "No se pudo guardar la imagen en la base de datos");
                return BadRequest(ModelState);
            }

            var root = HttpContext.Current.Server.MapPath("~/Images");
            var provider = new MultipartFormDataStreamProvider(root);
            Producto producto;
            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);

                producto = JsonConvert.DeserializeObject<Producto>(provider.FormData["producto"], new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                //var claimsIdentity = User.Identity as ClaimsIdentity;
                //producto.IdUsuario = claimsIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

                using (var unitOfWork = new UnitOfWork(this.WippinContext))
                {
                    //Editar!!!

                    //
                    if (producto != null)
                    {
                        if (provider.FileData.Count > 0)
                        {
                            foreach (var fileData in provider.FileData)
                            {
                                var imagen = new Imagen { Url = ImageService.UploadImage(fileData) };
                                producto.Imagenes.Add(imagen);
                            }
                        }
                        unitOfWork.Productos.Add(producto);
                        unitOfWork.Complete();
                    }
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                return BadRequest(ModelState);
            }
            return Ok(producto);

        }

        [SwaggerResponse(typeof(void))]
        [HttpDelete]
        [Route("{id:long}", Name = "DeleteProdById")]
#pragma warning disable 1998
        public async Task<IHttpActionResult> Delete(long id)
#pragma warning restore 1998
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                var producto = unitOfWork.Productos.Get(id);
                if (producto == null) return NotFound();
                unitOfWork.Productos.Remove(producto);
                unitOfWork.Complete();
            }
            return Ok();
        }
        [SwaggerResponse(typeof(void))]
        [HttpPost]
        [Route("visto/{id:long}", Name = "AddVistaWithIdProducto")]
        public async Task<IHttpActionResult> AddVista(long id)
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                var vista = new Vista();
                var producto = await unitOfWork.Productos.GetAsync(id);
                if (producto == null) return Ok();

                vista.Producto = producto;
                unitOfWork.Vistas.Add(vista);
                unitOfWork.Complete();
            }
            return Ok();
        }

    }

    public class FilterParams
    {
        public string Texto { get; set; }

        public Ubicacion Ubicacion { get; set; }

        public long? IdCategoria { get; set; }

        public bool? Ascendant { get; set; }
    }
}
