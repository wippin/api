﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using WippinApi.Core.Entities;
using WippinApi.Persistence;

namespace WippinApi.Controllers
{
    [RoutePrefix("api/transacciones")]
    public class TransaccionesController : BaseApiController
    {
        [HttpGet]
        [Route("", Name = "GetTransactions")]
#pragma warning disable 1998
        public async Task<IHttpActionResult> GetAll()
#pragma warning restore 1998
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                return Ok(unitOfWork.Transacciones.GetAll());
            }
        }


        [HttpPost]
        [Route("{idProducto:long}", Name = "AddTransaction")]
        [Authorize]
        public async Task<IHttpActionResult> Create(long idProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                try
                {
                    #region encontrar idUsuarioConsumidor
                    string idUsuarioConsumidor = "";
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    foreach (var item in claimsIdentity.Claims)
                    {
                        if (item.Type == ClaimTypes.NameIdentifier)
                        {
                            idUsuarioConsumidor = item.Value;
                            break;
                        }
                    }
                    if (idUsuarioConsumidor.Equals(""))
                    {
                        ModelState.AddModelError("", "Usuario logueado no encontrado.");
                        return BadRequest(ModelState);
                    }
                    #endregion
                    var producto = unitOfWork.Productos.Get(idProducto);
                    if (producto == null) return NotFound();
                    var transaccion = new Transaccion();
                    transaccion.IdUsuarioConsumidor = idUsuarioConsumidor;
                    transaccion.IdUsuarioPrestador = producto.IdUsuario;
                    transaccion.Titulo = producto.Titulo;
                    transaccion.Precio = producto.Precio;
                    transaccion.Descripcion = producto.Descripcion;
                    transaccion.FechaTransaccion = DateTime.Now;
                    transaccion.FechaEstimada = transaccion.FechaTransaccion;
                    unitOfWork.Transacciones.Add(transaccion);
                    unitOfWork.Complete();
                    return Ok(transaccion.Id);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                    return BadRequest(ModelState);
                }
            }
        }

        [HttpPost]
        [Route("{id:long}/valoracion", Name = "AddValoracion")]
        [Authorize]
        public async Task<IHttpActionResult> AddValoracion(long id, Valoracion valoracion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                try
                {
                    valoracion.Transaccion = unitOfWork.Transacciones.Get(id);
                    if (valoracion.Transaccion == null) return NotFound();
                    unitOfWork.Valoraciones.Add(valoracion);
                    unitOfWork.Complete();
                    return Ok(valoracion.Id);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                    return BadRequest(ModelState);
                }
            }
        }

        //todo transacciones por usuario

        //

        //
    }
}