﻿using NSwag.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using WippinApi.Core.Entities;
using WippinApi.Persistence;

namespace WippinApi.Controllers
{
    [RoutePrefix("api/ciudades")]
    public class CiudadesController : BaseApiController
    {
        [SwaggerResponse(typeof(IEnumerable<Ciudad>))]
        [HttpGet]
        [Route("", Name = "GetCiudades")]
        public async Task<IHttpActionResult> GetAll()
        {
            using (var unitOfWork = new UnitOfWork(this.WippinContext))
            {
                return Ok(unitOfWork.Ciudades.GetAll());
            }
        }
    }
}
