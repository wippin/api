﻿using ImageProcessor;
using ImageProcessor.Imaging;
using System;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Web.Hosting;

namespace WippinApi.Services
{
    public class ImageService
    {
        public static void CompressImage(string fileName, FileInfo fi)
        {
            using (var inStream = new MemoryStream(File.ReadAllBytes(fileName)))
            {
                using (var outStream = new MemoryStream())
                {
                    using (var imageFactory = new ImageFactory())
                    {
                        int width = imageFactory.Load(inStream).Image.Width;
                        int height = imageFactory.Load(inStream).Image.Height;
                        int qualityFactor = 60;
                        if (width >= 2500)
                        {
                            width = width / 2;
                            height = height / 2;
                            imageFactory.Load(inStream).Resize(new System.Drawing.Size(width, height)).Quality(qualityFactor).Save(outStream);
                        }
                        else
                        {
                            imageFactory.Load(inStream).Quality(qualityFactor).Save(outStream);

                        }
                    }

                    FileStream fs = fi.OpenWrite();
                    fs.SetLength(outStream.Length);
                    outStream.CopyTo(fs);
                    fs.Close();
                }
            }
        }

        private static void CompressMiniImage(string fileName, FileInfo fi)
        {

            using (var inStream = new MemoryStream(File.ReadAllBytes(fileName)))
            {
                using (var outStream = new MemoryStream())
                {
                    using (var imageFactory = new ImageFactory())
                    {
                        int qualityFactor = 60;
                        imageFactory.Load(inStream).Resize(new ResizeLayer(new System.Drawing.Size(320, 225), ResizeMode.Min)).Quality(qualityFactor).Save(outStream);
                    }

                    FileStream fs = fi.OpenWrite();
                    fs.SetLength(outStream.Length);
                    outStream.CopyTo(fs);
                    fs.Close();
                }
            }
        }
        public static string GetServerUrl()
        {
            if (ConfigurationManager.AppSettings["app:UrlImages"] == null)
            {
                throw new Exception("Falta definir app:UrlImages en el archivo de configuracion");
            }
            return ConfigurationManager.AppSettings["app:UrlImages"];
        }
        public static string GetServerUrl(string appendToTheEnd)
        {
            if (ConfigurationManager.AppSettings["app:UrlImages"] == null)
            {
                throw new Exception("Falta definir app:UrlImages en el archivo de configuracion");
            }
            return ConfigurationManager.AppSettings["app:UrlImages"] + appendToTheEnd;
        }

        public static string UploadImage(MultipartFileData fileData)
        {


            FileInfo fi = new FileInfo(fileData.LocalFileName);
            //string fullName = string.Empty;
            var clientFileName = fileData.Headers.ContentDisposition.FileName.Replace(@"""", "");
            var fileName = string.Format("{0}_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"),
                clientFileName.Replace(' ', '_')); //fCasabona
            var filePath = HostingEnvironment.MapPath("~/images");
            if (filePath != null)
            {
                var fname = Path.Combine(filePath, fileName);
                fi.MoveTo(fname);

                CompressImage(fname, fi); //AGuibarra

                return GetServerUrl("/images/") + fileName;
            }

            return "";
        }

        public static string UploadMiniImage(MultipartFormDataStreamProvider provider)
        {
            if (provider.FileData.Count > 0)
            {
                MultipartFileData fileData = provider.FileData[0];
                FileInfo fi = new FileInfo(fileData.LocalFileName);
                //string fullName = string.Empty;
                var clientFileName = fileData.Headers.ContentDisposition.FileName.Replace(@"""", "");
                var fileName = string.Format("{0}_mini_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"),
                    clientFileName.Replace(' ', '_')); //fCasabona
                var filePath = HostingEnvironment.MapPath("~/images");
                if (filePath != null)
                {
                    var fname = Path.Combine(filePath, fileName);
                    fi.MoveTo(fname);
                    CompressMiniImage(fname, fi); //AGuibarra

                    return GetServerUrl("/images/") + fileName;
                }
            }
            return "";

        }
    }
}
