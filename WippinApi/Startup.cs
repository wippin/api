﻿

using Auth0.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Owin;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Web.Http;
using WippinApi.Persistence;

[assembly: OwinStartup(typeof(WippinApi.Startup))]

namespace WippinApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureAuthZero(app);

            WebApiConfig.Register(config);
            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        private void ConfigureAuthZero(IAppBuilder app)
        {
            app.CreatePerOwinContext(WippinContext.Create);
            var domain = $"https://{ConfigurationManager.AppSettings["Auth0Domain"]}/";
            var apiIdentifier = ConfigurationManager.AppSettings["Auth0ApiIdentifier"];

            var keyResolver = new OpenIdConnectSigningKeyResolver(domain);

            app.UseJwtBearerAuthentication(
      new JwtBearerAuthenticationOptions
      {
          AuthenticationMode = AuthenticationMode.Active,
          TokenValidationParameters = new TokenValidationParameters()
          {
              ValidAudience = apiIdentifier,
              ValidIssuer = domain,
              IssuerSigningKeyResolver = (token, securityToken, identifier, parameters) => keyResolver.GetSigningKey(identifier),

          }

      });



        }

    }
}